package rs.dcloud.videotutorialsapi.security;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        //TODO: Implement custom logic to return current user name from security context
        return Optional.empty();
    }
}

