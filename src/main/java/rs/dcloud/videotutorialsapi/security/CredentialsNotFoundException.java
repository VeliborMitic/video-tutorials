package rs.dcloud.videotutorialsapi.security;

import org.springframework.security.core.AuthenticationException;

public class CredentialsNotFoundException extends AuthenticationException {
    public CredentialsNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }

    public CredentialsNotFoundException(String msg) {
        super(msg);
    }
}
