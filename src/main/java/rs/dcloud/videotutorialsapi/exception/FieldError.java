package rs.dcloud.videotutorialsapi.exception;

public class FieldError {
    private final String field;
    private final Object rejectedValue;
    private final String message;

    public FieldError(String field, Object rejectedValue, String message) {
        this.field = field;
        this.rejectedValue = rejectedValue;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public Object getRejectedValue() {
        return rejectedValue;
    }

    public String getMessage() {
        return message;
    }
}
