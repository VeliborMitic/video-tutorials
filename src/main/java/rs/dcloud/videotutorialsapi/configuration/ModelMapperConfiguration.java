package rs.dcloud.videotutorialsapi.configuration;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Date;
import java.time.Instant;

@Configuration
public class ModelMapperConfiguration {

    @Bean
    public ModelMapper getModelMapper() {
        ModelMapper modelMapper = new ModelMapper();

        //TODO: create type mappings here

        return modelMapper;
    }

    private Converter<Instant, String> instantToPrettyTimeConverter(){
        return mappingContext -> {
            if (mappingContext.getSource() != null)
                return new PrettyTime().format(Date.from(mappingContext.getSource()));
            return null;
        };
    }

}
