package rs.dcloud.videotutorialsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VideoTutorialsApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(VideoTutorialsApiApplication.class, args);
    }

}
